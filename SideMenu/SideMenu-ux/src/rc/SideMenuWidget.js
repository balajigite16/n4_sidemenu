
define(['baja!',
        'bajaux/mixin/subscriberMixIn',
        'bajaux/Widget',
        'jquery',
        'Promise',
        'bajaux/events',
        'underscore',
        'hbs!nmodule/SideMenu/rc/template/SideMenuTemplate',
        'nmodule/SideMenu/rc/scripts/bootstrap.bundle.min',
        'css!nmodule/SideMenu/rc/css/SideMenuWidget',
        'css!nmodule/SideMenu/rc/css/bootstrap',
        'css!nmodule/SideMenu/rc/css/bootstrap.min',
        ],function(
         baja,
         subscriberMixIn,
         Widget,
         $,
         Promise,
         events,
         _,
         template
        ){

        'use strict';
        var compSub = new baja.Subscriber(),
                      props,
                      field,
                      datasetValue,
                      arc,
                      gradient,min,max;
        var gap = 2;
        var ranDataset = function () {
          var ran = Math.random();

          return    [
            {index: 0, name: 'move', icon: "", percentage: ran * 60 + 30},
          ];

        };

        var ranDataset2 = function () {
          var ran = Math.random();

          return    [
            {index: 0, name: 'move', icon: "", percentage: ran * 60 + 30}
          ];

        };
        function gaugeProperties() {
            return [
              {name: 'min', value: '0', typeSpec: 'baja:Double'},
              {name: 'max', value: '100', typeSpec: 'baja:Double'}
            ];
        }

        var SideMenuWidget = function(){
             var that = this,
                     options;

             Widget.apply(that, arguments);

             that.options = options || {};
             that.options.margin = that.options.margin || {
               top: 30,
               right: 60,
               bottom: 45,
               left: 60
             };

             that.$title = null;
             that.margin = that.options.margin;

           this.properties().addAll(gaugeProperties());
           subscriberMixIn(this);
        };

        SideMenuWidget.prototype = Object.create(Widget.prototype);
        SideMenuWidget.prototype.constructor = SideMenuWidget;

        SideMenuWidget.prototype.doInitialize = function(dom){
          var that = this,
             prop = that.properties();
             min = prop.get("min").value,
             max = prop.get("max").value;
          var pie = 2 * Math.PI;
          var width = 400,
              height = 400;
          dom.html(template({}));

            var coll = $(".collapsible");
            var i;

            for (i = 0; i < coll.length; i++) {
              coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){
                  content.style.maxHeight = null;
                } else {
                  content.style.maxHeight = content.scrollHeight + "px";
                }
              });
            }

          $("#menuitemMainMenu").click(function(){
            $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
            $(".sidebar-levelone-item").removeClass("levelone-active");
            $(this).toggleClass("levelone-active");
            $("#MainMenu").toggleClass("leveltwo-expand");
          });

          $("#menuitemFloors").click(function(){
            $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
            $(".sidebar-levelone-item").removeClass("levelone-active");
            $(this).toggleClass("levelone-active");
            $("#Floors").toggleClass("leveltwo-expand");
          });

          $("#menuitemDOMWater").click(function(){
            $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
            $(".sidebar-levelone-item").removeClass("levelone-active");
            $(this).toggleClass("levelone-active");
            $("#DOMWater").toggleClass("leveltwo-expand");
          });

          $("#menuitemCooling").click(function(){
            $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
            $(".sidebar-levelone-item").removeClass("levelone-active");
            $(this).toggleClass("levelone-active");
            $("#Cooling").toggleClass("leveltwo-expand");
          });


          $(".sidebar-leveltwo-wrapper").on("mouseleave", function() {
            $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
          });


          $(".sidebar-leveltwo-item").click(function(){
            $(".sidebar-leveltwo-item").removeClass("leveltwo-active");
            $(this).toggleClass("leveltwo-active");

          });

        };

        SideMenuWidget.prototype.doLoad = function(comp){
           var that = this;
        };

    return SideMenuWidget;
});