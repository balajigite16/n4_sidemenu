$("#menuitemMainMenu").mouseover(function(){
  $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
  $(".sidebar-levelone-item").removeClass("levelone-active");
  $(this).toggleClass("levelone-active");
  $("#MainMenu").toggleClass("leveltwo-expand");
});

$("#menuitemFloors").mouseover(function(){
  $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
  $(".sidebar-levelone-item").removeClass("levelone-active");
  $(this).toggleClass("levelone-active");
  $("#Floors").toggleClass("leveltwo-expand");
});

$("#menuitemDOMWater").mouseover(function(){
  $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
  $(".sidebar-levelone-item").removeClass("levelone-active");
  $(this).toggleClass("levelone-active");
  $("#DOMWater").toggleClass("leveltwo-expand");
});

$("#menuitemCooling").mouseover(function(){
  $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
  $(".sidebar-levelone-item").removeClass("levelone-active");
  $(this).toggleClass("levelone-active");
  $("#Cooling").toggleClass("leveltwo-expand");
});


$(".sidebar-leveltwo-wrapper").on("mouseleave", function() {
  $(".sidebar-leveltwo-wrapper").removeClass("leveltwo-expand");
});


$(".sidebar-leveltwo-item").click(function(){
  $(".sidebar-leveltwo-item").removeClass("leveltwo-active");
  $(this).toggleClass("leveltwo-active");
});